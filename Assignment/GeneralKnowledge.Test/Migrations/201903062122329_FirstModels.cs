namespace GeneralKnowledge.Test.App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Assets",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FileName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assets", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MimeTypes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Type = c.String(),
                        CreatedBy = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assets", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MimeTypes", "Id", "dbo.Assets");
            DropForeignKey("dbo.Countries", "Id", "dbo.Assets");
            DropIndex("dbo.MimeTypes", new[] { "Id" });
            DropIndex("dbo.Countries", new[] { "Id" });
            DropTable("dbo.MimeTypes");
            DropTable("dbo.Countries");
            DropTable("dbo.Assets");
        }
    }
}
