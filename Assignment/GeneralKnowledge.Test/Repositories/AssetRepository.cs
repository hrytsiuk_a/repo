﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralKnowledge.Test.App.POCO;
using WebExperience.Test.EFContext;

namespace GeneralKnowledge.Test.App.Repositories
{
    public class AssetRepository : IAssetRepository
    {
        private DataAccess _db;
        public AssetRepository()
        {
            _db = new DataAccess();
        }

        public void Create(Asset asset)
        {
            _db.Assets.Add(asset);
            _db.SaveChanges();
        }

        public  void Delete(string id)
        {
            _db.Assets.Remove(GetById(id));
            _db.SaveChanges();
        }

        public  IEnumerable<Asset> GetAll()
        {
            return _db.Assets.Include(x => x.MimeType).Include(x => x.Country).ToList();
        }


        public  Asset GetById(string id)
        {
            return  _db.Assets.Include(x => x.MimeType).Include(x => x.Country).FirstOrDefault(x => x.Id == id);
        }

        public void Update(Asset asset)
        {
    
            _db.Entry(asset).State = EntityState.Modified;
            _db.Entry(asset.MimeType).State = EntityState.Modified;
            _db.Entry(asset.Country).State = EntityState.Modified;      
            _db.SaveChanges();
        }
    }
}
