﻿using GeneralKnowledge.Test.App.POCO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebExperience.Test.EFContext;

namespace GeneralKnowledge.Test.App.Repositories
{
    public class DbInitializer : DropCreateDatabaseAlways<DataAccess>
    {
        protected override void Seed(DataAccess context)
        {
            context.Assets.AddRange(GetAssets().Take(20));
            context.SaveChanges();
            base.Seed(context);
        }


        private List<Asset> GetAssets()
        {
            Regex regex = new Regex(@"\"".*?\""");

            var csvFile = Resources.AssetImport;
            List<Asset> assets = new List<Asset>();
            List<string> csv = csvFile.Split('\n').Skip(1).ToList();

            foreach (var model in csv)
            {
                if (string.IsNullOrEmpty(model)) continue;
                var d = model;
                List<string> regexValues = RegexValues(regex.Matches(model));
                string[] properties = ReplaceValues(model, regexValues);
                assets.AddNewAsset(properties);
            }

            var k = assets.Where(x => string.IsNullOrEmpty(x.Country.Description)).ToList();

            return assets;
        }


        #region Private functions

        private List<string> RegexValues(MatchCollection matchCollection)
        {
            List<string> regexValues = new List<string>();
            foreach (Match item in matchCollection)
                regexValues.Add(item.Value);

            return regexValues;
        }

        private string[] ReplaceValues(string model, List<string> values)
        {
            string[] splitedValues = null;
            foreach (var item in values)
                model = model.Replace(item, string.Empty);

            splitedValues = model.Split(',');

            if (splitedValues.Where(x => string.IsNullOrEmpty(x)).Count() > 0)
            {
                for (int i = 0; i < splitedValues.Length; i++)
                {
                    int k = 0;
                    if (string.IsNullOrEmpty(splitedValues[i]))
                    {
                        splitedValues[i] = values[k];
                        k++;
                    }
                }
            }
 
            return splitedValues;
        }

        #endregion Private functions
    }
}
