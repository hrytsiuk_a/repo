﻿using GeneralKnowledge.Test.App.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Repositories
{
    public interface IAssetRepository
    {
        IEnumerable<Asset> GetAll();
        Asset GetById(string id);
        void Delete(string id);
        void Create(Asset asset);
        void Update(Asset asset);
    }
}
