﻿using System;
using System.ComponentModel.DataAnnotations;


namespace GeneralKnowledge.Test.App.POCO
{
    public class Asset
    {
        public Asset() { }

        public Asset(string id, string fileName)
        {
            Id = string.IsNullOrEmpty(id) ? Guid.NewGuid().ToString() : id;
            FileName = fileName;
        }

        [Key]
        public string Id { get; set; }
        public string FileName { get; set; }

        public virtual MimeType MimeType { get; set; }
        public virtual Country Country { get; set; }

    }
}
