﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.POCO
{
    public class MimeType
    {
        public MimeType() { }

        public MimeType(string type, string createdBy, string email)
        {
            Id = Guid.NewGuid().ToString();
            Type = type;
            CreatedBy = createdBy;
            Email = email;
        }
        [Key]
        [ForeignKey("Asset")]
        public string Id { get; set; }
        public string Type { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }

        public Asset Asset { get; set; }
    }
}
