﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.POCO
{
    public class Country
    {
        public Country() { }

        public Country(string name, string description) {
            Id = Guid.NewGuid().ToString();
            Name = name;
            Description = description;
        }
        [Key]
        [ForeignKey("Asset")]
        public string Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public Asset Asset { get; set; }
    }
}
