﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.POCO
{
    class CommonJsonModel
    {
        public DateTime Date { get; set; }
        public double Temperature { get; set; }
        public int PH { get; set; }
        public int Chloride { get; set; }
        public int Phosphate { get; set; }
        public int Nitrate { get; set; }
    }
}
