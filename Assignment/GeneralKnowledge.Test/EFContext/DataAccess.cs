﻿using GeneralKnowledge.Test.App.POCO;
using GeneralKnowledge.Test.App.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebExperience.Test.EFContext
{
    public class DataAccess : DbContext
    {
        public DataAccess() : base("DatabaseConnection")
        {
            //Database.SetInitializer(new DbInitializer());
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Asset> Assets { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<MimeType> MimeTypes { get; set; }
    }
}