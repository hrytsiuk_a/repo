﻿using System;
using System.Drawing;


namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO
            // Grab an image from a public URL and write a function that rescales the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)

            Size startSize = new Size(100,80);

            Bitmap bitmap = new Bitmap(@"C:\Users\Andrew\Desktop\Test progjects — копия\GeneralKnowledge.Test\Resources\panda.bmp");
            bitmap = new Bitmap((Image)bitmap, startSize);

            Size outputSize = new Size(1200, 1600);
            Image outputImage = new Bitmap(bitmap, outputSize);
            outputImage.Save(@"C:\Users\Andrew\Desktop\Test progjects — копия\GeneralKnowledge.Test\Resources\pandaOutput.bmp");
        }
    }
}
