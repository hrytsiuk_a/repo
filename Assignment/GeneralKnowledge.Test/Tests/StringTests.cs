﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            Console.WriteLine("\n String test - Anagram Test \n");
            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private void GetUniqueCharsAndCount()
        {
            // TODO
            // Write an algorithm that gets the unique characters of the word below 
            // and counts the number of occurrences for each character found
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";
            char[] array = word.Distinct().ToArray();

            Dictionary<char, int> amountPerChar = new Dictionary<char, int>();

            Console.WriteLine("\n String test - Unique characters \n");
            foreach (var item in array)
            {
                int amount = word.Where(x => x == item).Count();
                amountPerChar.Add(item, amount);
                Console.WriteLine($"Char -{item}, Amount - {amount}");
            } 
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO
            // Write logic to determine whether a is an anagram of b

            char[] firtsWord = a.ToCharArray();
            char[] secondWord = b.ToCharArray();

            if (firtsWord.Length == secondWord.Length)
            {
                for (int i = 0; i < firtsWord.Length; i++)
                {
                    if (!secondWord.Any(x => x == firtsWord[i]))
                        return false;
                }
                return true;
            }
            return false;
        }
    }
}
