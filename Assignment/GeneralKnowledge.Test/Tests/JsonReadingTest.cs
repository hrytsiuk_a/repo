﻿using GeneralKnowledge.Test.App.POCO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic data retrieval from JSON test
    /// </summary>
    public class JsonReadingTest : ITest
    {
        public string Name { get { return "JSON Reading Test";  } }

        public void Run()
        {
            var jsonData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(jsonData);
        }

        private void PrintOverview(byte[] data)
        {

            List<CommonJsonModel> models = GetParsedJson(data);
            Console.WriteLine($"\n {Name} \n");
            WriteLine(CreateOutputString("parameter", "LOW", "AVG", "MAX"));
            WriteLine(CreateOutputString("temperature", models.Min(x => x.Temperature).ToString(), String.Format("{0:N2}", models.Average(x => x.Temperature)), models.Max(x => x.Temperature).ToString()));
            WriteLine(CreateOutputString("pH", models.Min(x => x.PH).ToString(), String.Format("{0:N2}", models.Average(x => x.PH)), models.Max(x => x.PH).ToString()));
            WriteLine(CreateOutputString("Chloride", models.Min(x => x.Chloride).ToString(), String.Format("{0:N2}", models.Average(x => x.Chloride)), models.Max(x => x.Chloride).ToString()));
            WriteLine(CreateOutputString("Phosphate", models.Min(x => x.Phosphate).ToString(), String.Format("{0:N2}", models.Average(x => x.Phosphate)), models.Max(x => x.Phosphate).ToString()));
            WriteLine(CreateOutputString("Nitrate", models.Min(x => x.Nitrate).ToString(), String.Format("{0:N2}", models.Average(x => x.Nitrate)), models.Max(x => x.Nitrate).ToString()));

        }

        private string CreateOutputString(params string[] parameters)
        {
            string result = string.Empty;
            foreach (var item in parameters)
                result += string.Format("{0,-20}", item);

            return result;
        }


        private List<CommonJsonModel> GetParsedJson(byte[] data)
        {
            List<CommonJsonModel> models = new List<CommonJsonModel>();
            var list = JObject.Parse(System.Text.Encoding.UTF8.GetString(data));
            IList<JToken> results = list["samples"].Children().ToList();

            foreach (var item in results)
                models.Add(item.ToObject<CommonJsonModel>());

            return models;
        }

    


    }
}
