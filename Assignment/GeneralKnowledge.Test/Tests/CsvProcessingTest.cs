﻿
using GeneralKnowledge.Test.App.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            Regex regex = new Regex(@"\"".*?\""");

            var csvFile = Resources.AssetImport;
            List<Asset> assets = new List<Asset>();
            List<string> csv = csvFile.Split('\n').Skip(1).ToList();

            foreach (var model in csv)
            {
                if (string.IsNullOrEmpty(model)) continue;

                List<string> regexValues = RegexValues(regex.Matches(model));
                string[] properties = ReplaceValues(model, regexValues);
                assets.AddNewAsset(properties);
            }
        }


        private List<string> RegexValues(MatchCollection matchCollection)
        {
            List<string> regexValues = new List<string>();
            foreach (Match item in matchCollection)
                regexValues.Add(item.Value);

            return regexValues;
        }

        private string[] ReplaceValues(string model, List<string> values)
        {
            string[] splitedValues = null;
            foreach (var item in values)
                model = model.Replace(item, string.Empty);

            splitedValues = model.Split(',');

            string[] emptyValues = splitedValues.Where(x => string.IsNullOrEmpty(x)).ToArray();

            if (emptyValues.Count() > 0)
            {
                for (int i = 0; i < emptyValues.Count(); i++)
                    emptyValues[i] = values[i];
            }

            return splitedValues;
        }
    }
}

public static class ExtensionHelper
{
    public static void AddNewAsset(this List<Asset> assets, string[] properties)
    {
        if (properties.Length == 7)
        {
            assets.Add(new Asset()
            {
                Id = properties[0],
                FileName = properties[1],
                MimeType = new MimeType(properties[2], properties[3], properties[4]),
                Country = new Country(properties[5], properties[6].Trim('"'))
            });
        }
    }
}




