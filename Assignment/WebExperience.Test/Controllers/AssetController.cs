﻿using GeneralKnowledge.Test.App.POCO;
using GeneralKnowledge.Test.App.Repositories;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebExperience.Test.CORS;

namespace WebExperience.Test.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class AssetController : ApiController
    {
        // TODO
        // Create an API controller via REST to perform all CRUD operations on the asset objects created as part of the CSV processing test
        // Visualize the assets in a paged overview showing the title and created on field
        // Clicking an asset should navigate the user to a detail page showing all properties
        // Any data repository is permitted
        // Use a client MVVM framework


        private IAssetRepository _repo;
        private JsonSerializerSettings _serialzier;
        public AssetController()
        {
            _repo = new AssetRepository();

            _serialzier = new JsonSerializerSettings();
            _serialzier.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        }

        [HttpGet]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetAssets()
        {
            IEnumerable<Asset> assets = _repo.GetAll();
            var json = JsonConvert.SerializeObject(assets, _serialzier);
            return Ok(json);

        }

        [HttpGet]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetAsset(string id)
        {
            Asset asset = _repo.GetById(id);
            var json = JsonConvert.SerializeObject(asset, _serialzier);
            return Ok(json);
        }

        [HttpPost]
        public IHttpActionResult CreateAsset(Asset asset)
        {
            if (asset == null)
            {
                return BadRequest($"Problem with adding Asset to database");
            }
            if (string.IsNullOrEmpty(asset.Id))
            {
                Asset temp = new Asset(asset.Id, asset.FileName);
                temp.Country = new Country(asset.Country.Name, asset.Country.Description);
                temp.MimeType = new MimeType(asset.MimeType.Type, asset.MimeType.CreatedBy, asset.MimeType.Email);
                _repo.Create(temp);
                return Ok();
            }
            else if (!string.IsNullOrEmpty(asset.Id))
            {
                _repo.Update(asset);
                return Ok();
            }
            return BadRequest("Problem with adding Asset to database");
        }


        [HttpPost]
        [ResponseType(typeof(bool))]
        [AllowCrossSite]
        public IHttpActionResult DeleteAsset(string id)
        {
            try
            {
                _repo.Delete(id);
                return Ok(true);
            }
            catch
            {
                return Ok(false);
            }
        }

    }
}
