﻿using System.Web.Optimization;

namespace WebExperience.Test
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/Script/Bundles").Include(
                  "~/bundles/runtime.*",
                  "~/bundles/zone.*",
                  "~/bundles/ployies.*",
                  "~/bundles/main.*"));
            bundles.Add(new StyleBundle("~/Content/Styles").Include("~/bundles/styles.*"));
        }
    }
}
