import { Asset } from "./../_models/asset";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AssetService {
  baseUrl = "http://localhost:62677/api/asset/";

  constructor(private http: HttpClient) {}

  getAssets(): Observable<string> {
    return this.http.get<string>(this.baseUrl);
  }

  getAsset(id: string): Observable<string> {
    return this.http.get<string>(this.baseUrl + id);
  }

  addAsset(asset: Asset) {
    return this.http.post(this.baseUrl, asset);
  }

  updateAsset( asset: Asset) {
    return this.http.post(this.baseUrl, asset);
  }

  deleteAsset(id: string) {
    return this.http.post(this.baseUrl + id, {});
  }
}
