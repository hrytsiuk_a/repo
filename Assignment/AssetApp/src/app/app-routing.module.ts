import { AssetEditComponent } from './asset/asset-edit/asset-edit.component';
import { AssetAddComponent } from "./asset/asset-add/asset-add.component";
import { AssetDetailComponent } from "./asset/asset-detail/asset-detail.component";
import { AssetsListComponent } from "./asset/assets-list/assets-list.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: AssetsListComponent },
  { path: "assets", component: AssetsListComponent },
  { path: "assets/:id", component: AssetDetailComponent },
  { path: "edit/:id", component: AssetEditComponent },

  { path: "addAsset", component: AssetAddComponent },
  { path: "**", redirectTo: "", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
