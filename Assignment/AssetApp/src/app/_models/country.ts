export interface Country {
  Id: string;
  Name: string;
  Description: string;
}
