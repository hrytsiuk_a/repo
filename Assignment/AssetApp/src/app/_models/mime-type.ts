export interface MimeType {
  Id: string;
  Type: string;
  CreatedBy: string;
  Email: string;
}
