import { MimeType } from './mime-type';
import { Country } from './country';

export interface Asset {
    Id: string;
    FileName: string;
    MimeType: MimeType;
    Country: Country;
}
