import { Router } from "@angular/router";
import { AssetService } from "./../../_services/asset.service";
import { Country } from "./../../_models/country";
import { Component, OnInit } from "@angular/core";
import { Asset } from "src/app/_models/asset";
import { MimeType } from "src/app/_models/mime-type";

@Component({
  selector: "app-asset-add",
  templateUrl: "./asset-add.component.html",
  styleUrls: ["./asset-add.component.css"]
})
export class AssetAddComponent implements OnInit {
  asset = {} as Asset;
  mime = {} as MimeType;
  country = {} as Country;

  constructor(private service: AssetService, private router: Router) {}

  ngOnInit() {}

  createAsset() {
    this.map();



    this.service.addAsset(this.asset).subscribe(res => {
      this.router.navigate(["/assets"]);
    });
  }

  map() {
    this.asset.Country = this.country;
    this.asset.MimeType = this.mime;
  }
}
