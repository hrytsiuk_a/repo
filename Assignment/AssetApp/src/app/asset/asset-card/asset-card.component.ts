import { Component, OnInit, Input } from '@angular/core';
import { Asset } from 'src/app/_models/asset';

@Component({
  selector: 'app-asset-card',
  templateUrl: './asset-card.component.html',
  styleUrls: ['./asset-card.component.css']
})
export class AssetCardComponent implements OnInit {

  @Input() asset: Asset;
  constructor() { }

  ngOnInit() {
  }

}
