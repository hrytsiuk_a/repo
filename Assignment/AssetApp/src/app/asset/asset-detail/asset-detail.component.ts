import { AssetService } from "./../../_services/asset.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Asset } from "src/app/_models/asset";

@Component({
  selector: "app-asset-detail",
  templateUrl: "./asset-detail.component.html",
  styleUrls: ["./asset-detail.component.css"]
})
export class AssetDetailComponent implements OnInit {
  asset: Asset;
  id: string;
  constructor(
    private route: ActivatedRoute,
    private service: AssetService,
    private router: Router
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");
    this.getAsset();
  }

  getAsset() {
    this.service.getAsset(this.id).subscribe((asset: string) => {
      this.asset = JSON.parse(asset);
    });
  }

  removeAsset() {
    this.service.deleteAsset(this.id).subscribe((response: boolean) => {
      if (response) {
        this.router.navigateByUrl("/assets");
      }
    });
  }
}
