import { AssetService } from "./../../_services/asset.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Asset } from "src/app/_models/asset";

@Component({
  selector: "app-asset-edit",
  templateUrl: "./asset-edit.component.html",
  styleUrls: ["./asset-edit.component.css"]
})
export class AssetEditComponent implements OnInit {
  asset: Asset;
  constructor(
    private router: Router,
    private service: AssetService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getAsset(this.route.snapshot.paramMap.get("id"));
  }

  getAsset(id: string) {
    this.service.getAsset(id).subscribe((asset: string) => {
      this.asset = JSON.parse(asset);
    });
  }

  updateAsset() {
    this.service.updateAsset( this.asset).subscribe(res => {
      this.router.navigate(['/assets']);
    });
  }

}
