import { Asset } from "./../../_models/asset";
import { AssetService } from "./../../_services/asset.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-assets-list",
  templateUrl: "./assets-list.component.html",
  styleUrls: ["./assets-list.component.css"]
})
export class AssetsListComponent implements OnInit {
  p = 1;
  assets: Asset[];

  constructor(private service: AssetService) {}

  ngOnInit() {
    this.getAssets();
  }

  getAssets() {
    this.service.getAssets().subscribe((list: string) => {
      this.assets = JSON.parse(list);
    });
  }
}
