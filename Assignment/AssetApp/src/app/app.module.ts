import { AssetAddComponent } from './asset/asset-add/asset-add.component';
import { HttpClientModule } from '@angular/common/http';
import { AssetEditComponent } from './asset/asset-edit/asset-edit.component';
import { AssetDetailComponent } from './asset/asset-detail/asset-detail.component';
import { AssetCardComponent } from './asset/asset-card/asset-card.component';
import { AssetsListComponent } from './asset/assets-list/assets-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';


@NgModule({
   declarations: [
      AppComponent,
      AssetsListComponent,
      AssetCardComponent,
      AssetEditComponent,
      AssetDetailComponent,
      NavigationComponent,
      AssetAddComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      NgxPaginationModule,
      FormsModule
   ],
   providers: [
      {provide: APP_BASE_HREF, useValue: ''}
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }